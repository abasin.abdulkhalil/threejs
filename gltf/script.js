// SCENE
var scene = new THREE.Scene();
scene.background = new THREE.Color(0x090909);


// CAMERA
var camera = new THREE.PerspectiveCamera( 100, window.innerWidth / window.innerHeight, 1, 900 );
camera.position.set(5,15,5);


// RENDERER
var renderer = new THREE.WebGLRenderer( { antialias: true } );
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement);


// Camera Rotation
var controls = new THREE.OrbitControls( camera );

controls.rotateSpeed = 1;
controls.zoomSpeed = 0.9;

controls.minDistance = 3;
controls.maxDistance = 20;


//lighting

light = new THREE.PointLight(0xc4c4c4,1);
light.position.set(0,300,500);
scene.add(light);
light2 = new THREE.PointLight(0xc4c4c4,1);
light2.position.set(500,100,0);
scene.add(light2);
light3 = new THREE.PointLight(0xc4c4c4,1);
light3.position.set(0,100,-500);
scene.add(light3);
light4 = new THREE.PointLight(0xc4c4c4,1);
light4.position.set(-500,300,500);
scene.add(light4);



// LOAD GFTL        https://sketchfab.com/feed
var loader = new THREE.GLTFLoader();

loader.crossOrigin = true;

loader.load( 'face/scene.gltf', function ( gltf ) {
    var object = gltf.scene;
    // object.scale.set(5,5,5);
    scene.add( object );
});


function render () {
  requestAnimationFrame( render ); 
  renderer.render( scene, camera );
  controls.update();
}

render();

window.addEventListener( 'resize', function () {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight );
}, false );