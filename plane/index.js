window.addEventListener('load', init, false);

function init() {
	createScene();
	createLights();
	createPlane();
	loop();
}



function createScene() {

	scene = new THREE.Scene();
  	camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 1000 );
	camera.position.x = 0;
	camera.position.z = 200;
	camera.position.y = 100;
  	scene.rotation.x = 0.3;
  	scene.rotation.y = 0.6;
	
	renderer = new THREE.WebGLRenderer({
    alpha: true,
		antialias: true 
	});

	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.shadowMap.enabled = true;

	container = document.getElementById('airplane');
	container.appendChild(renderer.domElement);

	window.addEventListener('resize', handleWindowResize, false);
}

function createLights() {
	hemisphereLight = new THREE.HemisphereLight(0xaaaaaa,0x000000, .9)
	shadowLight = new THREE.DirectionalLight(0xffffff, .9); 
	shadowLight.position.set(150, 350, 350);
	shadowLight.castShadow = true;

	shadowLight.shadow.camera.left = -400;
	shadowLight.shadow.camera.right = 400;
	shadowLight.shadow.camera.top = 400;
	shadowLight.shadow.camera.bottom = -400;

	shadowLight.shadow.mapSize.width = 2048;
	shadowLight.shadow.mapSize.height = 2048;
	
	scene.add(hemisphereLight);  
	scene.add(shadowLight);
}


var AirPlane = function() {
	
	this.mesh = new THREE.Object3D();

	var geomCockpit = new THREE.BoxGeometry(200,90,50,1,1,1);
	var matCockpit = new THREE.MeshPhongMaterial({color:0x35530a, shading:THREE.FlatShading});
	var cockpit = new THREE.Mesh(geomCockpit, matCockpit);
	cockpit.castShadow = true;
	cockpit.receiveShadow = true;
	this.mesh.add(cockpit);
	
	var geomEngine = new THREE.BoxGeometry(100,60,50,1,1,1);
	var matEngine = new THREE.MeshPhongMaterial({color:0xcae6f3, shading:THREE.FlatShading});
	var engine = new THREE.Mesh(geomEngine, matEngine);
	engine.position.x = 50;
  	engine.position.y = 15;
	engine.castShadow = true;
	engine.receiveShadow = true;
	this.mesh.add(engine);
	
	var geomTailPlane = new THREE.BoxGeometry(170,50,50,1,1,1);
	var matTailPlane = new THREE.MeshPhongMaterial({color:0x35530a, shading:THREE.FlatShading});

	geomTailPlane.vertices[4].y-=10;
	geomTailPlane.vertices[4].z+=20;
	geomTailPlane.vertices[5].y-=10;
	geomTailPlane.vertices[5].z-=20;
	geomTailPlane.vertices[6].y+=30;
	geomTailPlane.vertices[6].z+=20;
	geomTailPlane.vertices[7].y+=30;
	geomTailPlane.vertices[7].z-=20;


	var tailPlane = new THREE.Mesh(geomTailPlane, matTailPlane);
	tailPlane.position.set(-180,13,0);
	tailPlane.castShadow = true;
	tailPlane.receiveShadow = true;
	this.mesh.add(tailPlane);
	
	var geomPropeller = new THREE.BoxGeometry(5,60,5,1,1,1);
	var matPropeller = new THREE.MeshPhongMaterial({color:0x59332e, shading:THREE.FlatShading});
	this.propeller = new THREE.Mesh(geomPropeller, matPropeller);
	this.propeller.castShadow = true;
	this.propeller.receiveShadow = true;

	var geomBlade = new THREE.BoxGeometry(300,5,20,1,1,1);
	var matBlade = new THREE.MeshPhongMaterial({color:0x23190f, shading:THREE.FlatShading});
	
	var blade = new THREE.Mesh(geomBlade, matBlade);
	blade.position.set(8,32,0);
	blade.castShadow = true;
	blade.receiveShadow = true;
	this.propeller.add(blade);
	this.propeller.position.set(30,50,0);
	this.mesh.add(this.propeller);

	var geomPropeller2 = new THREE.BoxGeometry(5,35,5,1,1,1);
	var matPropeller2 = new THREE.MeshPhongMaterial({color:0x59332e, shading:THREE.FlatShading});
	this.propeller2 = new THREE.Mesh(geomPropeller2, matPropeller2);
	this.propeller2.castShadow = true;
	this.propeller2.receiveShadow = true;

	var geomBlade2 = new THREE.BoxGeometry(100,5,10,1,1,1);
	var matBlade2 = new THREE.MeshPhongMaterial({color:0x23190f, shading:THREE.FlatShading});
	
	var blade2 = new THREE.Mesh(geomBlade2, matBlade2);
	blade2.position.set(10,15,0);
	blade2.castShadow = true;
	blade2.receiveShadow = true;
	this.propeller2.add(blade2);
	this.propeller2.position.set(-260,35,0);
	this.mesh.add(this.propeller2);
};

function createPlane(){ 
	airplane = new AirPlane();
	airplane.mesh.scale.set(.25,.25,.25);
	airplane.mesh.position.y = 100;
	scene.add(airplane.mesh);
}

function loop(){
	airplane.propeller.rotation.y += 0.3;
  	airplane.propeller2.rotation.y += 0.3;
	renderer.render(scene, camera);
	requestAnimationFrame(loop);
}

function handleWindowResize() {
	renderer.setSize(window.innerWidth, window.innerHeight);
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
}